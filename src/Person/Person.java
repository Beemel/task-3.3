package Person;

public class Person {
    private String personName;
    private int heightInCentimeter;
    private String personGender;
    private String personHometown;

//constructor
    public Person(String personName, int heightInCentimeter, String personGender, String personHometown) {
        this.personName = personName;
        this.heightInCentimeter = heightInCentimeter;
        this.personGender = personGender;
        this.personHometown = personHometown;
    }


//operations/behaviours = methods, Things the class can do operations to be performed
//in general fields private methods public

    public String getHometown() {
        return this.personHometown;
    }

//Choose greeting type

    public String versionsOfGreetings(Person firstPerson) {

        if (firstPerson.personHometown == personHometown) {

            return "Hello Friend, I seen ya round, i see we are both from " +personHometown+".";
        }
        else {
            return "Howdy partner!" + System.lineSeparator() + System.lineSeparator() +
                    "Ma name be " + personName + " , it be a tootin' pleasure to make ya acquaintance" + System.lineSeparator() + System.lineSeparator() +
                    "I be an odd stick, who be burnin' ma prairie coal at a fine and dandy place call'd " + personHometown + " a.k.a. redneck central o' the wild east! " + System.lineSeparator() +
                    "I spend ma sun laps far away from the choke straps and tenderfoots of big towns, just how i like it. " + System.lineSeparator() +
                    "As my old Ma', God rest her soul, used to say about the cities (life is like a box o' cho.... no.. hollup' a moment, thats another story....) "+ System.lineSeparator() +
                    "'Never approach a bull from the front, a horse from the rear or a fool from any direction' " +System.lineSeparator() +
                    "I wouldn't wanna be one to put too much mustard on by poor lonesom' being, but this turned out one apple pie order of a greeting!";


        }
    }
    public double heightInInch (){
        return (heightInCentimeter*0.39370079);
    }


    public int heightInCentimeter() {
        return heightInCentimeter;
    }

    public String personName(){
        return personName;
    }

    public String lengthConversion(){
      return "The height of " + personName() + " is " + heightInCentimeter() + " in centimeters, which is equal to "+ heightInInch()+ "inch";
        }

}