import Person.Person;

import java.awt.*;
import java.util.logging.MemoryHandler;

public class Main {

    public static void main(String[] args) {

        Person firstPerson = new Person ("Theresa Hillbilly", 175, "female", "Smallertown");
        Person secondPerson = new Person ("Roger Cowpusher", 190, "male", "Smalltown");
        Person thirdPerson = new Person ("Savannah Barwench", 152, "female", "Smalltown");


        System.out.println(thirdPerson.versionsOfGreetings(firstPerson));
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println(secondPerson.lengthConversion());

    }


    }

